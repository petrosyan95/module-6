<?php

$textStorage = [];

function add( array &$testStorage, string $title, string $text) : void
{
    $testStorage[] = ["title" => $title, "text" => $text];
}

add($textStorage, "title", "text");

add($textStorage, "header", "body");

var_dump($textStorage);

function remove( int $index, array &$textStorage) : bool
{
    if (array_key_exists($index, $textStorage)) {
        unset($textStorage[$index]);
        return true;
    }
    return false;
}

var_dump(remove(0, $textStorage));

var_dump(remove(5, $textStorage));

var_dump($textStorage);

function edit( int $index, array &$textStorage, string $newTitle = null, string $newText = null) : bool
{
    if (array_key_exists($index, $textStorage)) {
        $textStorage[$index]['title'] = $newTitle ?? $textStorage[$index]['title'];
        $textStorage[$index]['text'] = $newText ?? $textStorage[$index]['text'];
        return true;
    }
    return false;
}

edit(1, $textStorage, null, "lorem");

var_dump($textStorage);

var_dump(edit(2, $textStorage, 'hed',));


